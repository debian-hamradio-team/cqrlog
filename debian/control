Source: cqrlog
Section: hamradio
Priority: optional
Maintainer: Debian Hamradio Maintainers <debian-hams@lists.debian.org>
Uploaders:
 Petr Hlozek <petr@ok2cqr.com>,
Build-Depends:
 debhelper-compat (= 13),
 fp-units-db,
 fp-units-gfx,
 fp-units-gtk2,
 fp-units-math,
 fp-units-misc,
 fp-units-net,
 fp-utils,
 lazarus | lazarus-0.9.30,
 lcl | lcl-0.9.30,
 libatk1.0-dev,
 libcairo2-dev,
 libgdk-pixbuf-2.0-dev,
 libgtk2.0-dev,
 libhamlib-dev,
 libpango1.0-dev,
 libx11-dev,
Standards-Version: 4.7.0
Homepage: https://www.cqrlog.com
Vcs-Browser: https://salsa.debian.org/debian-hamradio-team/cqrlog
Vcs-Git: https://salsa.debian.org/debian-hamradio-team/cqrlog.git
Rules-Requires-Root: no

Package: cqrlog
Architecture: any
Depends:
 cqrlog-data (= ${source:Version}),
 mariadb-client-core,
 mariadb-server-core,
 libhamlib-utils,
 ${hamlib:Depends},
 libmariadb-dev-compat,
 ${misc:Depends},
 ${shlibs:Depends},
Breaks: cqrlog-data (<< 2.5.2-2)
Replaces: cqrlog-data (<< 2.5.2-2)
Recommends: mariadb-server, xplanet
Description: Advanced logging program for hamradio operators
 CQRLOG is an advanced ham radio logger based on MySQL embedded database.
 Provides radio control based on hamlib libraries (currently support of 140+
 radio types and models), DX cluster connection, HamQTH/QRZ callbook
 (XML access), a grayliner, internal QSL manager database support and a most
 accurate country resolution algorithm based on country tables developed by
 OK1RR. CQRLOG is intended for daily general logging of HF, CW & SSB contacts
 and strongly focused on easy operation and maintenance.

Package: cqrlog-data
Architecture: all
Multi-Arch: foreign
Depends:
 hamradio-files,
 ${misc:Depends},
# Breaks/Replaces can be removed during the Trixie release cycle when
# version 2.5.1 in Bullseye becomes oldstable.
Breaks: cqrlog (<< 2.4.0)
Replaces: cqrlog (<< 2.4.0)
Description: Advanced logging program for hamradio operators (data files)
 CQRLOG is an advanced ham radio logger based on MySQL embedded database.
 Provides radio control based on hamlib libraries (currently support of 140+
 radio types and models), DX cluster connection, HamQTH/QRZ callbook
 (XML access), a grayliner, internal QSL manager database support and a most
 accurate country resolution algorithm based on country tables developed by
 OK1RR. CQRLOG is intended for daily general logging of HF, CW & SSB contacts
 and strongly focused on easy operation and maintenance.
 .
 This package contains the architecture-independent data files.
